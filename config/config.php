<?php
/**
 * Created by PhpStorm.
 * User: Mutasov Roman
 * Date: 28.05.2016
 * Time: 10:18
 */

$app['locale'] = 'ru';

//$app['debug'] = true;

$app['db.options'] = [
    'driver'   => 'pdo_mysql',
    'host'     => 'localhost',
    'dbname'   => 'mutasov_test',
    'user'     => 'root',
    'password' => '',
    'charset'  => 'utf8',
];

$app['twig.path'] = __DIR__ . '/../App/Templates';

$app['upload.options'] = [
    'upload_dir'                => __DIR__ . '/../web/uploads',
    'allowed_images_mime_types' => [
        'image/bmp', 'image/x-bmp', 'image/x-ms-bmp',
        'image/jpeg', 'image/pjpeg',
        'image/png',
        'image/gif',
    ]
];


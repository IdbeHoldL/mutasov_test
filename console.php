<?php

require_once __DIR__ . '/vendor/autoload.php';

/* @var $app Silex\Application */
$app = require __DIR__ . '/AppConsole/app.php';

require __DIR__ . '/config/config.php';

use AppConsole\Command\PostExportCommand;

$application = $app['console'];
$application->add(new PostExportCommand());
$application->run();

//$consoleApp = $app['console'];
//$consoleApp->add(new \AppConsole\Command\PostExportCommand());
//
//$consoleApp->run();
//$app->run();
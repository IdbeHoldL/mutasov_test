<?php
/**
 * Created by PhpStorm.
 * User: Mutasov Roman
 * Date: 28.05.2016
 * Time: 16:15
 */

namespace App\Repository;


class PostImageRepository extends BaseRepository
{
    /**
     * @param $postId
     * @param $name
     * @param $filename
     * @return string
     */
    public function add($postId, $name, $filename)
    {
        $this->db->insert('post_image', [
            'post_id'  => $postId,
            'name'     => $name,
            'filename' => $filename,
        ]);

        return $this->db->lastInsertId();
    }
}
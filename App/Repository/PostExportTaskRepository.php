<?php
/**
 * Created by PhpStorm.
 * User: Mutasov Roman
 * Date: 29.05.2016
 * Time: 0:45
 */

namespace App\Repository;


class PostExportTaskRepository extends BaseRepository
{

    /**
     * @param $postId
     * @return string
     */
    public function add($postId)
    {
        $this->db->insert('post_export_task', [
            'post_id' => $postId,
        ]);

        return $this->db->lastInsertId();
    }

    /**
     * @param $id
     * @param $serviceResponse
     * @return bool
     * @throws \Doctrine\DBAL\DBALException
     */
    public function markExported($id, $serviceResponse)
    {

        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->update('post_export_task', 'pet')
            ->set('pet.is_new', '0')
            ->set('pet.service_response', '?')
            ->where('pet.id = ?');

        return ($this->db->executeQuery($queryBuilder->getSQL(), [$serviceResponse, $id])) ?: false;
    }

}
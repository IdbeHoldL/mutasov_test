<?php
/**
 * Created by PhpStorm.
 * User: Mutasov Roman
 * Date: 28.05.2016
 * Time: 15:25
 */

namespace App\Repository;

class PostRepository extends BaseRepository
{


    /**
     * @param $name
     * @param $message
     * @return string
     * @throws \Doctrine\DBAL\DBALException
     */
    public function add($name, $message)
    {
        $this->db->insert('post', [
            'name'    => $name,
            'message' => $message,
        ]);

        return $this->db->lastInsertId();
    }


    /**
     * @param $id
     * @return bool|mixed
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getById($id)
    {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select([
                'p.*',
                'group_concat(pi.name SEPARATOR 0x1D) as image_names',
                'group_concat(pi.filename SEPARATOR 0x1D) as image_filenames',
            ])
            ->from('post', 'p')
            ->leftJoin('p', 'post_image', 'pi', 'p.id = pi.post_id')
            ->where('p.id = ?')
            ->groupBy('p.id');


        $postData = $this->db->executeQuery($queryBuilder->getSQL(), [$id])->fetch(\PDO::FETCH_ASSOC);

        return ($postData) ? $this->format($postData) : false;
    }

    /**
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getAll()
    {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select([
                'p.*',
                'group_concat(pi.name SEPARATOR 0x1D) as image_names',
                'group_concat(pi.filename SEPARATOR 0x1D) as image_filenames',
            ])
            ->from('post', 'p')
            ->leftJoin('p', 'post_image', 'pi', 'p.id = pi.post_id')
            ->orderBy('p.created_at', 'DESC')
            ->groupBy('p.id');

        $postDatas = $this->db->executeQuery($queryBuilder->getSQL())->fetchAll(\PDO::FETCH_ASSOC);

        return array_map(function ($value) {
            return $this->format($value);
        }, $postDatas);
    }

    /**
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getNotExported()
    {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select([
                'pet.id as post_export_task_id',
                'p.*',
                'group_concat(pi.name SEPARATOR 0x1D) as image_names',
                'group_concat(pi.filename SEPARATOR 0x1D) as image_filenames',
            ])
            ->from('post_export_task', 'pet')
            ->leftJoin('pet', 'post', 'p', 'pet.post_id = p.id')
            ->leftJoin('p', 'post_image', 'pi', 'p.id = pi.post_id')
            ->where('pet.is_new = 1')
            ->groupBy('p.id');

        $postDatas = $this->db->executeQuery($queryBuilder->getSQL())->fetchAll(\PDO::FETCH_ASSOC);

        return array_map(function ($value) {
            return $this->format($value);
        }, $postDatas);
    }

    /**
     * @param $postData
     * @return mixed
     */
    private function format($postData)
    {
        $imageNames     = isset($postData['image_names']) ? explode(chr(0x1D), $postData['image_names']) : [];
        $imageFileNames = isset($postData['image_names']) ? explode(chr(0x1D), $postData['image_filenames']) : [];

        unset($postData['image_names']);
        unset($postData['image_filenames']);

        $postData['images'] = [];
        foreach ($imageFileNames as $key => $imageFilename) {
            $postData['images'][] = [
                'name'     => isset($imageNames[$key]) ? $imageNames[$key] : '',
                'filename' => $imageFilename,
            ];
        }

        return $postData;
    }

}
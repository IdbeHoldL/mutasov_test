<?php
/**
 * Created by PhpStorm.
 * User: Mutasov Roman
 * Date: 28.05.2016
 * Time: 14:30
 */

namespace App\Repository;

use Doctrine\DBAL\Connection;


abstract class  BaseRepository
{

    /**
     * @var Connection
     */
    protected $db;

    /**
     * @param Connection $db
     */
    public function __construct(Connection $db)
    {
        $this->db = $db;
    }

}
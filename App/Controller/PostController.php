<?php
/**
 * Created by PhpStorm.
 * User: Mutasov Roman
 * Date: 28.05.2016
 * Time: 11:03
 */

namespace App\Controller;

use Silex\Application;
use Silex\Route;
use Silex\Api\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormError;

class PostController implements ControllerProviderInterface
{

    /**
     * @param Application $app
     * @return \Silex\ControllerCollection
     */
    public function connect(Application $app)
    {

        /* @var $index \Silex\ControllerCollection */
        $index = $app['controllers_factory'];


        /**
         * Форма добавления поста
         */
        $index->match('/add', function (Application $app, Request $request) {

            /* @var $formBuilder \Symfony\Component\Form\FormBuilder */
            $formBuilder = $app['form.factory']->createBuilder(FormType::class)
                ->add('name', TextType::class, ['label' => 'ФИО', 'required' => true])
                ->add('message', TextareaType::class, ['label' => 'Текст сообщения', 'required' => true])
                ->add('image', FileType::class, ['label' => 'Приложить изображение', 'required' => false])
                // валидация
                ->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) use ($app) {
                    $form = $event->getForm();

                    if (!$form->get('name')->getData()) {
                        $form['name']->addError(new FormError("Пожалуйста, заполните это поле."));
                    }

                    $message = $form->get('message')->getData();
                    if (!$message || strlen($message) < 10) {
                        $form['message']->addError(new FormError("Текст сообщения не должен быть меньше 10 символов."));
                    }

                    /* @var $image \Symfony\Component\HttpFoundation\File\UploadedFile */
                    if ($image = $form->get('image')->getData()) {
                        if (!in_array($image->getClientMimeType(), $app['upload.options']['allowed_images_mime_types'])) {
                            $form['image']->addError(new FormError("Неверный формат файла."));
                        }
                    }
                });

            $form = $formBuilder->getForm();
            $form->handleRequest($request);

            if ($form->isValid()) {

                $formData = $form->getData();

                $app['db']->beginTransaction();
                try {
                    // добавляем пост
                    $postId = $app['repository.post']->add($formData['name'], $formData['message']);

                    // переносим картику и добавляем запись в бд
                    /* @var $image \Symfony\Component\HttpFoundation\File\UploadedFile */
                    if ($image = $formData['image']) {
                        $fileName = md5(uniqid('upload_image_', true)) . '.' . $image->getClientOriginalExtension();
                        $image->move($app['upload.options']['upload_dir'], $fileName);
                        $app['repository.post_image']->add($postId, $image->getClientOriginalName(), $fileName);
                    }

                    // добавляем задачу (для отправки данных в стороний сервис)
                    $app['repository.post_export_task']->add($postId);

                    $app['db']->commit();
                } catch (\Exception $e) {
                    $app['db']->rollBack();
                    if (isset($fileName) && file_exists($app['upload.options']['upload_dir'] . DIRECTORY_SEPARATOR . $fileName)) {
                        unlink($app['upload.options']['upload_dir'] . DIRECTORY_SEPARATOR . $fileName);
                    }
                    throw $e;
                };

                return $app->redirect('/post/' . $postId);
            }

            return $app['twig']->render('/post/add.twig', ['form' => $form->createView()]);
        });


        /**
         * Список всех созданных постов
         */
        $index->get('/list', function (Application $app) {

            $posts = ($app['repository.post']->getAll() ?: []);

            return $app['twig']->render('/post/list.twig', ['posts' => $posts]);
        });


        /**
         * Отображение поста
         */
        $index->get('/{id}', function ($id, Application $app) {

            if (!$post = $app['repository.post']->getById($id)) {
                $app->abort(404);
            }

            return $app['twig']->render('/post/show.twig', ['post' => $post]);
        });

        return $index;
    }
}
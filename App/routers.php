<?php
/**
 * Created by PhpStorm.
 * User: Mutasov Roman
 * Date: 29.05.2016
 * Time: 12:41
 */

// создание и просмотр постов
$app->mount('/post', new App\Controller\PostController());

// заглушка (сторонний сервис)
$app->match('/other-service/post-save', function () {

//    var_dump($_POST);
//    var_dump($_FILES);

    return 'RESULT:' . (rand(0, 1) ? 'OK' : uniqid('FAIL, REASON: '));
});
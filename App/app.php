<?php
/**
 * Created by PhpStorm.
 * User: Mutasov Roman
 * Date: 28.05.2016
 * Time: 10:22
 */

use Silex\Application;
use Silex\Provider\DoctrineServiceProvider;
use Silex\Provider\FormServiceProvider;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\TranslationServiceProvider;
use App\Repository\PostRepository;
use App\Repository\PostImageRepository;
use App\Repository\PostExportTaskRepository;

$app = new Application();
$app->register(new DoctrineServiceProvider());
$app->register(new FormServiceProvider());
$app->register(new TwigServiceProvider());
$app->register(new TranslationServiceProvider());
$app['repository.post']             = function () use ($app) {
    return new PostRepository($app['db']);
};
$app['repository.post_image']       = function () use ($app) {
    return new PostImageRepository($app['db']);
};
$app['repository.post_export_task'] = function () use ($app) {
    return new PostExportTaskRepository($app['db']);
};

return $app;
<?php
/**
 * Created by PhpStorm.
 * User: Mutasov Roman
 * Date: 28.05.2016
 * Time: 9:36
 */

require_once __DIR__ . '/../vendor/autoload.php';

/* @var $app Silex\Application */
$app = require __DIR__ . '/../App/app.php';
require __DIR__ . '/../App/routers.php';
require __DIR__ . '/../config/config.php';

$app->run();

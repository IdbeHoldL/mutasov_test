<?php
/**
 * Created by PhpStorm.
 * User: Mutasov Roman
 * Date: 29.05.2016
 * Time: 13:22
 */

namespace AppConsole\Command;

use Ivoba\Silex\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class PostExportCommand extends Command
{

    protected function configure()
    {
        $this
            ->setName('export-posts')
            ->setDescription('Export new Posts.')
            ->addArgument(
                'uri',
                InputArgument::REQUIRED,
                'Import for a specific user'
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $uri = $input->getArgument('uri');

        $app = $this->getSilexApplication();

        $posts = $app['repository.post']->getNotExported();

        foreach ($posts as $post) {
            $data  = [
                'name' => $post['name'],
                'name' => $post['message']
            ];
            $files = [];
            foreach ($post['images'] as $imageData) {
                $files[$imageData['name']] = $app['upload.options']['upload_dir'] . DIRECTORY_SEPARATOR . $imageData['filename'];
            }

            /* @var $postRequest \AppConsole\PostRequest\CurlPostRequest|\AppConsole\PostRequest\SocketPostRequest|\AppConsole\PostRequest\StreamPostRequest| */
            $postRequest = $app['postRequest']->create('socket');

            try {
                $serviceResponse = $postRequest->send($uri, $data, $files);
                $app['repository.post_export_task']->markExported($post['post_export_task_id'], $serviceResponse);
            } catch (\Exception $e) {
                // do something
                $output->writeln("TASK {$post['post_export_task_id']} error: {$e->getMessage()}");
            }
        }
        $output->writeln($uri);

        return 0;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: Mutasov Roman
 * Date: 29.05.2016
 * Time: 11:29
 */

namespace AppConsole\PostRequest;

class PostRequestFactory
{

    /**
     * @param $name
     * @return CurlPostRequest|SocketPostRequest|StreamPostRequest
     * @throws \Exception
     */
    public function create($name)
    {

        switch (strtolower($name)) {

            case "curl":
                return $this->createCurlPostRequest();
                break;

            case "socket":
                return $this->createSocketPostRequest();
                break;

            case "stream":
                return $this->createStreamPostRequest();
                break;

        }

        throw new \Exception("$name - is not valid PostRequest type");
    }

    /**
     * @return CurlPostRequest
     */
    public function createCurlPostRequest()
    {
        return new CurlPostRequest();
    }

    /**
     * @return SocketPostRequest
     */
    public function createSocketPostRequest()
    {
        return new SocketPostRequest();
    }

    /**
     * @return StreamPostRequest
     */
    public function createStreamPostRequest()
    {
        return new StreamPostRequest();
    }


}
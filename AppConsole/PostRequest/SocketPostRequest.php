<?php
/**
 * Created by PhpStorm.
 * User: Mutasov Roman
 * Date: 29.05.2016
 * Time: 10:16
 */

namespace AppConsole\PostRequest;


class SocketPostRequest extends BasePostRequest
{
    /**
     * @param $uri
     * @param $postData
     * @param array $files
     * @return bool|string
     */
    public function send($uri, $postData, $files = [])
    {
        $content       = $this->getContent($postData, $files);
        $urlComponents = parse_url($uri);

        $request
            = "POST {$urlComponents['path']}  HTTP/1.1\n"
            . "Host:{$urlComponents['host']}\n"
            . "Content-Type: multipart/form-data; boundary={$this->boundary}\n"
            . "Content-Length: " . strlen($content) . "\n"
            . 'Connection: close'
            . "\n\n"
            . $content;

        $errno  = 0;
        $errstr = '';
        if (!($socket = fsockopen($urlComponents['host'], 80, $errno, $errstr, 15))) {
            throw new \RuntimeException("Can't open socket! error: {$errno} - {$errstr}");
        }

        fwrite($socket, $request);

        $headers = '';
        do {
            $headers .= fread($socket, 1);
        } while (!preg_match('/\\r\\n\\r\\n$/', $headers));

        if (!preg_match('~HTTP\/\d+\.\d+ (\d+)~m', $headers, $matches)) {
            throw new \RuntimeException("Wrong answer");
        }
        if ($matches[1] != 200) {
            throw new \RuntimeException("HTTP_CODE : " . $matches[1]);
        }
        $response = false;
        if (preg_match('/Content\\-Length:\\s+([0-9]*)\\r\\n/', $headers, $matches)) {
            $response = fread($socket, $matches[1]);
        }

        fclose($socket);

        return $response;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: Mutasov Roman
 * Date: 29.05.2016
 * Time: 10:17
 */

namespace AppConsole\PostRequest;


class StreamPostRequest extends BasePostRequest
{
    /**
     * @param $uri
     * @param $postData
     * @param array $files
     * @return string
     */
    public function send($uri, $postData, $files = [])
    {

        $context = stream_context_create(
            [
                'http' => [
                    'method'  => 'POST',
                    'header'  => 'Content-Type: multipart/form-data; boundary=' . $this->boundary,
                    'content' => $this->getContent($postData, $files)
                ]
            ]
        );

        if (!$response = file_get_contents($uri, false, $context)) {
            throw new \RuntimeException('Failed to open stream!');
        }

        return $response;
    }
}
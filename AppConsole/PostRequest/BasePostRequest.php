<?php
/**
 * Created by PhpStorm.
 * User: Mutasov Roman
 * Date: 29.05.2016
 * Time: 10:14
 */

namespace AppConsole\PostRequest;


abstract class  BasePostRequest
{

    /**
     * @var string
     */
    protected $boundary;

    public function __construct()
    {
        $this->boundary = uniqid();
    }

    /**
     * @param $uri
     * @param $postData
     * @param $files
     * @return mixed
     */
    abstract public function send($uri, $postData, $files = []);

    /**
     * @param $postData
     * @param $files
     * @return string
     */
    public function getContent($postData, $files)
    {
        $content = '';

        foreach ($postData as $key => $val) {
            $content .= "--{$this->boundary}\n";
            $content .= "Content-Disposition: form-data; name=\"{$key}\"\n\n[$val]\n";
        }

        foreach ($files as $fileName => $filePath) {

            $fileContents = file_get_contents($filePath);

            if ($fileContents === false) {
                throw new \RuntimeException("Can't get file contents: {$filePath}");
            }

            $baseFileName = basename($filePath);

            $content .= "--{$this->boundary}\n";
            $content .= "Content-Disposition: form-data; name=\"{$fileName}\"; filename=\"" . $baseFileName . "\"\n";
            $content .= "Content-Type: application/octet-stream\n";
            $content .= "Content-Transfer-Encoding: binary\n\n";
            $content .= "{$fileContents}\n";
        }

        $content .= "--{$this->boundary}--\n";

        return $content;
    }
}
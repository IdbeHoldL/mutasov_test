<?php
/**
 * Created by PhpStorm.
 * User: Mutasov Roman
 * Date: 29.05.2016
 * Time: 10:17
 */

namespace AppConsole\PostRequest;

class CurlPostRequest extends BasePostRequest
{

    /**
     * @param $uri
     * @param $postData
     * @param array $files
     * @return mixed
     */
    public function send($uri, $postData, $files = [])
    {
        foreach ($files as $fileName => $filePath) {
            $postData[$fileName] = '@' . $filePath;
        }

        $ch = curl_init($uri);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if (!$response = curl_exec($ch)) {
            throw new \RuntimeException(curl_error($ch));
        }

        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($httpCode !== 200) {
            throw new \RuntimeException("HTTP_CODE : {$httpCode}");
        }

        curl_close($ch);

        return $response;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: Mutasov Roman
 * Date: 28.05.2016
 * Time: 10:22
 */

use Silex\Application;
use Silex\Provider\DoctrineServiceProvider;
use App\Repository\PostRepository;
use App\Repository\PostImageRepository;
use App\Repository\PostExportTaskRepository;
use Ivoba\Silex\Provider\ConsoleServiceProvider;

use AppConsole\PostRequest\PostRequestFactory;


$app = new Application();
$app->register(new DoctrineServiceProvider());

$app->register(new ConsoleServiceProvider(), [
    'console.name'              => 'console-app',
    'console.version'           => '1.0.0',
    'console.project_directory' => __DIR__ . '/..'
]);


$app['postRequest'] = function () {
    return new PostRequestFactory();
};

$app['repository.post']             = function () use ($app) {
    return new PostRepository($app['db']);
};
$app['repository.post_image']       = function () use ($app) {
    return new PostImageRepository($app['db']);
};
$app['repository.post_export_task'] = function () use ($app) {
    return new PostExportTaskRepository($app['db']);
};

return $app;
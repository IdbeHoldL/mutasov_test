# README #

Используется микрофрейворк Silex c Doctrine DBAL и SymfonyForms
требуется версия PHP >= 5.5

### Установка ###
* **mutasov_test.sql** - содержит дами БД
* подтянуть зависимости через композер
* добавить в крон выполнение комманды **export-posts** (пример ниже)
```
#!

php console.php export-posts http://www.mutasov-test.local/other-service/post-save
```


### Описание ###
* [mutasov_test / AppConsole / PostRequest / ] - классы для отправки данных в сторонний сервис
* [ mutasov_test / App / Controller / PostController.php ] - контроллер. просмотр/создание поста, просмотр списка постов
* [ mutasov_test / AppConsole / Command / PostExportCommand.php  ] - отправка данных на сторониий сервис происходит тут
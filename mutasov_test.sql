-- MySQL Script generated by MySQL Workbench
-- 05/29/16 17:09:58
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mutasov_test
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mutasov_test
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mutasov_test` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `mutasov_test` ;

-- -----------------------------------------------------
-- Table `mutasov_test`.`post`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mutasov_test`.`post` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `message` MEDIUMTEXT NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mutasov_test`.`post_image`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mutasov_test`.`post_image` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `post_id` INT(11) UNSIGNED NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `filename` VARCHAR(255) NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk_post_image_post_id_idx` (`post_id` ASC),
  CONSTRAINT `fk_post_image_post_id`
    FOREIGN KEY (`post_id`)
    REFERENCES `mutasov_test`.`post` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mutasov_test`.`post_export_task`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mutasov_test`.`post_export_task` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `post_id` INT(11) UNSIGNED NOT NULL,
  `is_new` TINYINT(1) NOT NULL DEFAULT 1,
  `service_response` MEDIUMTEXT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk_post_export_task_post_id_idx` (`post_id` ASC),
  CONSTRAINT `fk_post_export_task_post_id`
    FOREIGN KEY (`post_id`)
    REFERENCES `mutasov_test`.`post` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
